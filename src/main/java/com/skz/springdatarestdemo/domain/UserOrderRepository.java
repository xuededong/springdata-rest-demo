package com.skz.springdatarestdemo.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(path = "orders")
public interface UserOrderRepository extends JpaRepository<UserOrder, String> {
}
