package com.skz.springdatarestdemo.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@EqualsAndHashCode(of = "id")
public class User {
    @Id
    @GeneratedValue
    private int id;
    private String username;
    private String birthday;
    private int gender;
    private String address;
}
