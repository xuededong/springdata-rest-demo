package com.skz.springdatarestdemo.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class UserOrder {
    @Id
    private String id;
    private String name;

    public static void main(String[] args) {
        System.out.println(UserOrder.class.getSimpleName().toLowerCase()+"s");
    }
}
