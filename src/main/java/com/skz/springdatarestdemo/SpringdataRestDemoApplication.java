package com.skz.springdatarestdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringdataRestDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringdataRestDemoApplication.class, args);
    }

}
